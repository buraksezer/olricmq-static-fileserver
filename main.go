// olricmq-static-fileserver
// Copyright (C) 2018  Burak Sezer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/buraksezer/olricmq/rpc/client"
	"github.com/buraksezer/olricmq/rpc/protocol"
	"golang.org/x/net/http2"
)

var (
	host        string
	name        string
	staticDir   string
	templateDir string
	logger      *log.Logger
	wg          sync.WaitGroup
)

type fileItem struct {
	Name        string
	Mode        string
	ModDate     time.Time
	ContentType string
	Size        int64
}

func main() {
	logger = log.New(os.Stdout, "", log.Ltime|log.Lshortfile)
	flag.StringVar(&host, "host", "", "Host to connect")
	flag.StringVar(&name, "name", "", "Client's name on the cluster")
	flag.StringVar(&staticDir, "fileRoot", "", "Static file directory which will be served outside")
	flag.StringVar(&templateDir, "templateDir", "", "The absolute path to your template folder")
	flag.Parse()

	wd, err := os.Getwd()
	if err != nil {
		logger.Fatal("Failed to get work dir:", err)
	}

	// Check variable health
	if host == "" {
		logger.Fatal("You have to give a host")
	}
	if name == "" {
		logger.Fatal("You have to give a name")
	}
	if staticDir == "" {
		staticDir = wd
	}
	if templateDir == "" {
		templateDir = path.Join(wd, "templates")
	}

	ctx, cancel := context.WithCancel(context.Background())
	// Wait for interrupt at background and call cancel() before exit.
	go waitForInterrupt(cancel)

	// Listen for incoming requests.
	errCh := make(chan error, 1)
	wg.Add(1)
	go listenAndServe(ctx, cancel, errCh)

	<-ctx.Done()
	logger.Println("[INF] Awaits to be completed ongoing requests")
	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
	}()

	select {
	case <-done:
		logger.Println("[INF] All goroutines has been stopped gracefully.")
	case <-time.After(10 * time.Second):
		logger.Println("[WARN] Some goroutines could not be stopped gracefully.")
	}

	err = <-errCh
	if err != nil {
		logger.Fatalf("[ERR] listenAndServe failed: %v", err)
	}
}

func waitForInterrupt(cancel context.CancelFunc) {
	defer cancel()

	shutDownChan := make(chan os.Signal)
	signal.Notify(shutDownChan, syscall.SIGTERM, syscall.SIGINT)
	s := <-shutDownChan
	logger.Printf("Catched signal: %s", s.String())
}

func listenAndServe(ctx context.Context, cancel context.CancelFunc, errCh chan error) {
	defer wg.Done()
	defer cancel()

	tr := &http2.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	cfg := &client.Config{
		Name:   name,
		Hosts:  []string{host},
		Client: &http.Client{Transport: tr},
	}
	rpc, err := client.New(cfg)
	if err != nil {
		logger.Fatal("Failed to create RPC client:", err)
	}

	localErrCh := make(chan error, 1)
	go func() {
		localErrCh <- rpc.ListenAndServe()
	}()

	// TODO: Reconsider this.
	defer rpc.Close()
	for {
		select {
		case <-ctx.Done():
			logger.Println("[INF] ListenAndServe terminated")
			errCh <- nil
			return
		case err := <-localErrCh:
			logger.Println("[INF] Stopped")
			if err != nil {
				errCh <- fmt.Errorf("Failed to serve: %s", err)
				return
			}
			errCh <- nil
			return
		case req := <-rpc.Requests:
			if req.Method == protocol.MethodPost {
				logger.Printf("[%s] Method not allowed on %s", req.Method, req.URL.String())
				if err = req.WriteResponse(http.StatusMethodNotAllowed, nil, nil); err != nil {
					logger.Println("response writer returned error:", err)
				}
				continue
			}
			// Dispatch the request.
			wg.Add(1)
			go makeResponse(req)
		}
	}
}

func renderTemplate(name string, data interface{}) (*bytes.Buffer, error) {
	var buf bytes.Buffer
	fname := name + ".html"
	tpath := path.Join(templateDir, fname)
	tmpl, err := template.ParseFiles(tpath)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse template: %s: %v", fname, err)
	}
	if err := tmpl.Execute(&buf, data); err != nil {
		return nil, fmt.Errorf("Failed to render template: %s: %v", fname, err)
	}
	return &buf, nil
}

func returnError(req *client.Request, statusCode int, tname string, data interface{}) {
	wr, err := renderTemplate(tname, data)
	if err != nil {
		logger.Printf("Err: %v", err)
		return
	}
	h := make(http.Header)
	h.Set("X-Olric-Trim-Content-Type", "text/html; charset=utf-8")
	if err := req.WriteResponse(statusCode, wr, h); err != nil {
		logger.Printf("[ERR] Failed to return error message: %v", err)
	}
}

func listDir(req *client.Request, file *os.File) {
	content, err := ioutil.ReadDir(file.Name())
	if err != nil {
		msg := fmt.Sprintf("Failed to open folder %s: %v", file.Name(), err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}

	files := []fileItem{}
	for _, item := range content {
		fl := fileItem{
			Name:    item.Name(),
			Mode:    item.Mode().String(),
			Size:    item.Size(),
			ModDate: item.ModTime(),
		}
		files = append(files, fl)
	}

	tpath := path.Join(templateDir, "index.html")
	tmpl, err := template.ParseFiles(tpath)
	if err != nil {
		msg := fmt.Sprintf("Failed to parse template index.html: %v", err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}
	var wr bytes.Buffer
	if err := tmpl.Execute(&wr, files); err != nil {
		msg := fmt.Sprintf("Failed to render template index.html: %v", err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}
	h := make(http.Header)
	h.Set("X-Olric-Trim-Content-Type", "text/html; charset=utf-8")
	if err := req.WriteResponse(http.StatusOK, &wr, h); err != nil {
		logger.Printf("[ERR] Failed to serve index.html: %v", err)
	}
}

func makeResponse(req *client.Request) {
	defer wg.Done()

	p := path.Join(staticDir, req.URL.String())
	if _, err := os.Stat(p); os.IsNotExist(err) {
		logger.Printf("[ERR] HTTP 404 %s cannot be found.", p)
		returnError(req, http.StatusNotFound, "404", p)
		return
	}

	file, err := os.Open(p)
	if err != nil {
		msg := fmt.Sprintf("Failed to open %s: %v", p, err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}
	defer file.Close()

	fi, err := file.Stat()
	if err != nil {
		msg := fmt.Sprintf("Failed to get metadata %s: %v", p, err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}
	if fi.IsDir() {
		listDir(req, file)
		return
	}

	h := make(http.Header)
	contentLength := strconv.FormatInt(fi.Size(), 10)
	h.Set("X-Olric-Trim-Content-Length", contentLength)

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)
	_, err = file.Read(buffer)
	if err != nil {
		msg := fmt.Sprintf("Failed to get file type %s: %v", p, err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}
	// Reset the read pointer if necessary.
	if _, err := file.Seek(0, 0); err != nil {
		msg := fmt.Sprintf("Failed to seek to beginning of the file: %s: %s", p, err)
		returnError(req, http.StatusInternalServerError, "500", msg)
		return
	}

	statusCode := http.StatusOK
	// Always returns a valid content-type and "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)
	h.Set("X-Olric-Trim-Content-Type", contentType)
	h.Set("X-Olric-Trim-Accept-Ranges", "bytes")
	if rangeh, ok := req.Headers["Range"]; ok {
		rg := strings.TrimPrefix(rangeh[0], "bytes=")
		parsed, err := parseRange(rg, fi.Size())
		if err != nil {
			msg := fmt.Sprintf("Failed to get parse Range header %s: %v", p, err)
			returnError(req, http.StatusInternalServerError, "500", msg)
			return
		}
		contentLength := strconv.FormatInt(parsed[0].length, 10)
		h.Set("X-Olric-Trim-Content-Length", contentLength)
		contentRange := fmt.Sprintf("bytes %d-%d/%d", parsed[0].start, parsed[0].start+parsed[0].length-1, fi.Size())
		h.Set("X-Olric-Trim-Content-Range", contentRange)
		statusCode = http.StatusPartialContent
		file.Seek(parsed[0].start, 0)
	}

	logger.Printf("[INF] HTTP %d Serving: %s", statusCode, p)
	if req.Method == http.MethodHead {
		err = req.WriteResponse(statusCode, nil, h)
	} else {
		err = req.WriteResponse(statusCode, file, h)
	}
	if err != nil {
		logger.Printf("Failed to call WriteResponse: %v", err)
	}
}
